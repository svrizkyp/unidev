<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_VisualMerch
 */


namespace Amasty\VisualMerch\Plugin\Catalog\Model\ResourceModel;

use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\Store;

class Product
{
    /**
     * @var \Magento\Catalog\Model\Indexer\Category\Product\Processor
     */
    private $categoryProductIndexerProcessor;

    public function __construct(
        \Magento\Catalog\Model\Indexer\Category\Product\Processor $categoryProductIndexerProcessor
    ) {
        $this->categoryProductIndexerProcessor = $categoryProductIndexerProcessor;
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product $subject
     * @param $result
     * @return mixed
     */
    public function afterSave(\Magento\Catalog\Model\ResourceModel\Product $subject, $result)
    {
        $this->categoryProductIndexerProcessor->markIndexerAsInvalid();
        return $result;
    }

}
