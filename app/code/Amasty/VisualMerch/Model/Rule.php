<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_VisualMerch
 */


namespace Amasty\VisualMerch\Model;

class Rule extends \Magento\CatalogRule\Model\Rule
{
    /**
     * @var \Amasty\Base\Model\Serializer|null
     */
    protected $serializer;

    protected function _construct()
    {
        $amastySerializer = $this->getData('amastySerializer');
        if ($amastySerializer) {
            $this->serializer = $amastySerializer;
        }

        parent::_construct();
    }

    /**
     * @param \Magento\Framework\DB\Select $select
     * @return $this
     */
    public function applyAttributesFilter(\Magento\Framework\DB\Select $select)
    {
        $conditions = $this->getConditions();
        if ($conditions instanceof \Amasty\VisualMerch\Model\Rule\Condition\Combine) {
            $this->setAggregator($conditions->getAggregator());
            $conditions->collectValidatedAttributes($select);
            $condition = $conditions->collectConditionSql();
            if (!empty($condition)) {
                $select->where($condition);
            }
            $select->group('e.entity_id');
        }

        return $this;
    }
}
